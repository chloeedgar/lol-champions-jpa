package com.champions.springexercise1.repository;

import com.champions.springexercise1.entity.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepository extends JpaRepository <LolChampion, Long> {

}
