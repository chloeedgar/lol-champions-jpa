package com.champions.springexercise1.service;

import com.champions.springexercise1.entity.LolChampion;
import com.champions.springexercise1.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LolChampionService {

    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<LolChampion> findAll() {
        return this.lolChampionRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion) {
        return this.lolChampionRepository.save(lolChampion);
    }
}
